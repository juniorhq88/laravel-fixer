var konfio = konfio || {};
endpoint = 'latest';
access_key = 'f0769894df73c136386743758e7a2524';//Api Key

konfio.pageLoad =
{
	elementLoad:function()
  	{
  		konfio.events.loadLatestRate();
  		konfio.events.loadHistoricalRate();
  	}
};

konfio.events = 
{
	loadLatestRate:function(){
		$.ajax({
		    url: 'http://data.fixer.io/api/' + endpoint + '?access_key=' + access_key,   
		    dataType: 'jsonp',
		    success: function(json) {

		        // exchange rata data is stored in json.rates
		       // alert(json.rates.MXN);
		        
		        // base currency is stored in json.base
		        //alert(json.base);
		        
		        // timestamp can be accessed in json.timestamp
		       // alert(json.timestamp);
		        
		        $('#base').text(json.base);
		        $('#currency').text(json.rates.MXN);

		        
		    }
		});
	},
	loadHistoricalRate:function(){
		// La moneda base actual es el Euro (EUR), en la opción free de fixer.io no me permite cambiarlo a MXN
		$.ajax({
		    url: 'http://data.fixer.io/api/latest?access_key=' + access_key + '&symbols=USD,CAD,GBP,MXN,EUR&base=EUR',   
		    dataType: 'jsonp',
		    success: function(json) {
		    	if(json.success){
		    		var chart = c3.generate({
					    data: {
					        columns: [
					            ['EUR', parseFloat(json.rates.EUR)],
					            ['USD', parseFloat(json.rates.USD)],
					            ['CAD', parseFloat(json.rates.CAD)],
					            ['GBP', parseFloat(json.rates.GBP)],
					            ['MXN', parseFloat(json.rates.MXN)],

					        ],
					        type: 'bar'
					    },
					    bar: {
					        width: {
					            ratio: 0.5 // this makes bar width 50% of length between ticks
					        }
					        // or
					        //width: 100 // this makes bar width 100px
					    }
					});
		    	}
		         $('#currentDate').text(json.date);
		    }
		});
	}
};

$(document).ready(function()
{
  konfio.pageLoad.elementLoad();
});