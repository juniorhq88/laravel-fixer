<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel Rates</title>

        <!-- Fonts -->
        <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/app.css')) }}">

    </head>
    <body>
        <div class="flex-center position-ref">
            <div class="content">
                <table class="table table-hovered">
                    <thead>
                        <th>Base</th>
                        <th>Currency MXN</th>
                        <th>Date Today</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td><span id="base"></span></td>
                            <td><span id="currency"></span></td>
                            <td><span id="currentDate"></span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div id="chart"></div>
            </div>
        </div>
        <script src="{{ asset(mix('js/app.js')) }}" type="text/javascript"></script>
    </body>
</html>
