const mix = require('laravel-mix'),
	assetsDir   = 'resources/',
  	nodeDir     = 'node_modules/',
  	publicDir   = 'public/',
  	distDir     = 'public/',
  	composerDir = 'vendor/';

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
 applicationJs = [
 	nodeDir   + 'jquery/dist/jquery.min.js',
 	nodeDir   + 'd3/dist/d3.min.js',
 	assetsDir   + '/plugins/c3/c3.min.js',
    assetsDir + 'js/konfio.js'
  ];


mix.scripts(applicationJs, distDir + 'js/app.js')
   .sass(assetsDir + '/sass/app.scss', distDir + 'css/app.css');

if (mix.inProduction()) {
  mix.version();
}

