# Proyecto Laravel - Fixer

Proyecto de Laravel en su version 5.7 que permite consumir la API Fixer.io para los intercambios de Monedas.

## Instrucciones para instalar la aplicación

```sh
$ composer install
```

Copiamos el .env.example a .env donde pondremos los datos de accesos y conexión a la BD

```sh
$ cp .env.example .env
$ php artisan key:generate
$ php artisan migrate (Comando para instalar la Base de Datos)
$ php artisan db:seed (Para que se instalen los datos de ejemplos)

$ npm i
$ npm run watch (para compilar y minificar los archivos sass 
$ y js en public y mantener el node en espera de nuevos cambios)
$ npm run dev (para compilar y minificar los archivos sass y js en public)
$ npm run prod (para compilar y minificar los archivos sass y 
$ js en public en producción)
$ php artisan serve (para levantar el servidor local)
```

## Archivos principales modificados para la ejecución del proyecto:
 - Controlador: root/app/Http/Controllers/RateController.php
 - Vista: root/resources/views/home.blade.php
 - Js: root/resources/js/konfio.js
 - Sass: root/resources/sass/app.scss
 - Webpack: root/webpack.mix.js
## Autor

* **Junior Hernandez** - [Email](mailto:juniorhq88@gmail.com)